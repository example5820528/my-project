import os, sys, time

path = os.path.dirname(os.path.realpath(__file__))
parentpath = os.path.abspath(os.path.join(path, os.pardir))
modulespath = parentpath + '\Modules'
os.chdir(modulespath)
sys.path.append(modulespath)
name_test = r'КПЭ ВС (ФКУ): Рабочее место руководителя'

from GetLog import Logs
from Chrome import Browser
from ZabbixSend import ZabbixSend
from Times import Times
from Settings import RMR_Pass, RMR_User

# переменная окружения - от значения зависит место хранения логов, варианты 'dev' и 'prom'
env = 'prom'

# Web
url = r'https://rmr.nalog.ru/'
chrome = Browser(url, env)
file_name = os.path.splitext(os.path.basename(__file__))[0]

# Zabbix, Logs, Times
send = ZabbixSend(env)
logs = Logs(file_name, env)
times = Times()

def start():
    """Чистка лога в CMD
    Запуск браузера в полноэкранном режиме
    Переход на страницу сервиса
    Ожидание загрузки страницы до 60 секунд
    Проверка отображения страницы авторизации по строке `Логин`
    Если страница не загружается
    завершение скрипта с сообщением '###### Cервис https://rmr.nalog.ru/ недоступен  ######'"""
    logs.clear()
    tmp_msg = f'###### Вход на сервис {url} ######'
    chrome.start()
    try:
        chrome.get_url()
        chrome.browser.implicitly_wait(60)
        chrome.waitfor_id_el('username')
    except:
        raise Exception(f'### Cервис {url} недоступен ###')
    else:
        logs.info(tmp_msg)

def login():
    """Ввод логина
    Ввод пароля
    Нажатие кнопки 'Вход'
    Ожидание кликабельности элемента `Поступления` до 60 секунд
    Если страница не загружается
    завершение скрипта с сообщением 'Не выполнена Авторизация под "test_fku"' """
    tmp_msg = f'Авторизация под "{RMR_User}"'
    try:
        chrome.browser.find_element_by_id('username').send_keys(RMR_User)
        chrome.browser.find_element_by_id('password').send_keys(RMR_Pass)
        chrome.click_id('kc-login')
        chrome.waitfor_click_xpath('//div[text()="Поступления"]', 60)
    except:
        raise Exception(f'Не выполнена Авторизация под "{tmp_msg}"')
    else:
        logs.info(tmp_msg)

def cb():
    """ Клик на вкладку "Поступления"
     Во вкладке "Консолидированный бюджет РФ" нажатие на "Отображение информации в табличном виде"
     Проверка наличия таблицы до 60 сек
     Если таблица не загрузилась
     завершение скрипта с сообщением 'КБ РФ Информация в табличном виде не загрузилась'
     --------      
     Во вкладке "Консолидированный бюджет РФ" нажатие на "Отображение информации в виде интерактивной карты"
     Нажатие на элемент карты "Сибирский ФО"
     Ожидание прогрузки элемента карты "Томская область" до 30 секунд
     Если режимы не загрузились
     завершение скрипта с сообщением 'КБ РФ Информация в виде интерактивной карты не загрузилась'"
    --------
     Нажатие на элемент карты - "Томская область"
     Ожидание отображения текста "Налог на прибыль" до 30 секунд
     Ожидание отображения текста "НДФЛ" до 30 секунд
     Если режимы не загрузились
     завершение скрипта с сообщением 'КБ РФ статистика ФО не загрузилась'"""
    tmp_msg = "Консолидированный бюджет РФ"
    # table
    try:
        chrome.click_xpath('//div[text()="Поступления"]')
        chrome.click_xpath('//div[@data-testid="automaticViewModeButton_4"]')
        chrome.waitfor_xpath_el('//div[text()="ВСЕГО ДОХОДОВ"]', 60)
    except:
        raise Exception('КБ РФ Информация в табличном виде не загрузилась')
    # map
    try:
        chrome.click_xpath('//div[@data-testid="automaticViewModeButton_3"]')
        chrome.click_xpath('//div[text()="Сибирский ФО"]')
        chrome.waitfor_click_xpath('//div[text()="Томская область"]', 30)
    except:
        raise Exception('КБ РФ Информация в виде интерактивной карты не загрузилась')
    # statistics
    try:
        chrome.click_xpath('//div[text()="Томская область"]')
        chrome.waitfor_xpath_el('//div[text()="Налог на прибыль"]', 30)
        chrome.waitfor_xpath_el('//div[text()="НДФЛ"]', 30)
    except:
        raise Exception('КБ РФ статистика ФО не загрузилась')
    else:
        logs.info(tmp_msg)

def federal_budget():
    """Переход в раздел "Федеральный бюджет"
    Нажатие на 1 переключатель вида отображения информации
    Ожидание отображения строки "Начало периода" до 30 секунд
    Нажатие на 2 переключатель вида отображения информации
    Ожидание отображения блока "РФ" до 30 секунд
    Если режимы не загрузились
    завершение скрипта с сообщением 'Вкладка "Федеральный бюджет" недоступна'"""
    tmp_msg = 'Федеральный бюджет'
    try:
        chrome.click_xpath('//div[@dir="auto"][text()="ФЕДЕРАЛЬНЫЙ БЮДЖЕТ"]')
        chrome.click_xpath('//div[@data-testid="automaticViewModeButton_0"]') #1 переключатель
        chrome.waitfor_xpath_el('//div[@dir="auto"][text()="Начало периода"]', 30)
        chrome.click_xpath('//div[@data-testid="automaticViewModeButton_1"]') #2 переключатель
        chrome.waitfor_xpath_el('//div[@dir="auto"][text()="РФ"]', 30)
    except:
        raise Exception(tmp_msg)
    logs.info(tmp_msg)

def cb_rf_sub():
    """ Переход в раздел "КБ СУБЪЕКТОВ РФ"
        Нажатие на "Отображение информации в табличном виде"
        Нажатие на раздел "Исполнение"
        Ожидание отображения строки "Кассовое исполнение" до 30 секунд
        Нажатие на строку "Центральный федеральный округ"
        Ожидание отображения строки "Имущественные налоги" до 30 секунд """
    tmp_msg = 'КБ Субъектов РФ'
    try:
        chrome.click_xpath('//div[@dir="auto"][text()="КБ СУБЪЕКТОВ РФ"]')
        chrome.click_xpath('//div[@data-testid="automaticViewModeButton_4"]')
        chrome.click_xpath('//div[@dir="auto"][text()="ИСПОЛНЕНИЕ"]')
        chrome.waitfor_xpath_el('//div[text()="Кассовое исполнение"]', 30)
        chrome.click_xpath('//div[@dir="auto"][text()="Центральный федеральный округ"]')
        chrome.waitfor_xpath_el('//div[text()="Имущественные налоги"]', 30)
    except:
        raise Exception(tmp_msg)
    else:
        logs.info(tmp_msg)

def insurance():
    """ Переход в раздел "Страховые взносы"
        Ожидание строки "СТРАХОВЫЕ ВЗНОСЫ НА ОБЯЗАТЕЛЬНОЕ СОЦИАЛЬНОЕ СТРАХОВАНИЕ" до 30 секунд """
    tmp_msg = 'Страховые взносы'
    try:
        chrome.click_xpath('//div[@dir="auto"][text()="СТРАХОВЫЕ ВЗНОСЫ"]')
        chrome.waitfor_xpath_el('//div[@dir="auto"][text()="(Центральный федеральный округ)"]', 30)
    except:
        raise Exception(tmp_msg)
    else:
        logs.info(tmp_msg)

def smi():
    """ В нижнем левом углу нажатие кнопки "Разделы"
        Нажатие на раздел "СМИ"
        Ожидание строки "Анализ информационного поля ФНС России в СМИ" до 30 секунд
        Нажатие на блок «Негативный инфоповод»
        Ожидание строки "Негативный инфоповод" до 10 секунд"""
    tmp_msg = 'СМИ'
    try:
        chrome.click_class("css-1rynq56.r-1m5u4oz.r-lrvibr") #Разделы
        chrome.browser.find_elements_by_xpath("//div[text()='СМИ']")[1].click() #СМИ
        chrome.waitfor_xpath_el('//div[@dir="auto"][text()="Анализ информационного поля ФНС России в СМИ"]', 30)
        chrome.click_xpath("//div[text()='Негативный инфоповод']")
        chrome.waitfor_xpath_el('//div[@dir="auto"][text()="Негативный инфоповод"]', 10)
    except:
        raise Exception(tmp_msg)
    else:
        logs.info(tmp_msg)

def risks_map():
    """ В нижнем левом углу нажатие кнопки "Разделы"
        Нажатие на раздел "Карта рисков"
        Ожидание строки "Топ 5 из рейтинга рисков по Российской Федерации (худшие)" до 10 секунд
        Нажатие на элемент карты "Уральский ФО"
        Пвуза 0,5 сек
        Нажатие на элемент карты "Ханты-Мансийский автономный округ - Югра"
        Ожидание строки во всплывающем окне "Рейтинг риска по:" до 30 секунд
        Закрытие всплывающего окна """
    tmp_msg = 'Карта рисков'
    try:
        chrome.click_class("css-1rynq56.r-1m5u4oz.r-lrvibr") #Разделы
        chrome.browser.find_elements_by_xpath("//div[text()='Карта рисков']")[1].click() #Карта рисков
        chrome.waitfor_xpath_el('//div[@dir="auto"][text()="Топ 5 из рейтинга рисков по Российской Федерации (худшие)"]', 10)
        chrome.click_xpath('//div[@dir="auto"][text()="Уральский ФО"]')
        times.sleep(0.5)
        chrome.browser.find_elements_by_xpath('//div[text()="Ханты-Мансийский автономный округ - Югра"]')[1].click()
        chrome.waitfor_xpath_el('//div[@dir="auto"][text()="Рейтинг риска по:"]', 30)
        chrome.click_class('css-1rynq56.r-rhqcz8.r-qwp1k9.r-utggzx.r-atwnbb') #Закрытие всплыв окна
    except:
        raise Exception(tmp_msg)
    else:
        logs.info(tmp_msg)

def exit_lk():
    """ Нажатие на ссылку «ФНС Мониторинг»
        Ожидание главной страницы, строки 'Структура РМР' 5 секунд
        Нажатие на меню
        Нажатие на 'Выйти'
        Ожидание страницы авторизации по строке 'Логин'"""
    tmp_msg = 'Выход из ЛК'
    try:
        chrome.click_class('css-175oi2r.r-1i6wzkk.r-lrvibr.r-1loqt21.r-1otgn73') #ФНС Мониторинг
        chrome.waitfor_click_xpath('//div[@dir="auto"][text()="Структура РМР"]', 5) #Структура
        chrome.click_class('css-1rynq56.r-bnwqim.r-lrvibr') #Меню
        chrome.click_xpath("//div[text()='Выйти']") #Выйти
        chrome.waitfor_click_id('username', 10) #Логин
    except:
        raise Exception(tmp_msg)
    else:
        logs.info(tmp_msg)

def exit():
    tmp_msg = 'Выход'
    chrome.quit()
    logs.info(tmp_msg)
    logs.other()

def main():
    """Последовательно запускаем все вышеперечисленные функции
    Если в какой-то из них происходит ошибка, то делаем скрин, пишем лог с ошибкой
    В любом случае чистим старые скриншоты, отправляем файл в заббикс и ставим функцию на паузу"""
    try:
        start()
        login()
        cb()
        federal_budget()
        cb_rf_sub()
        insurance()
        smi()
        risks_map()
        exit_lk()
        exit()
    except Exception as err:
        chrome.screen(file_name)
        print('ОШИБКА - ' + str(err).replace('\n', ''))
        chrome.quit()
        logs.error(str(err).replace('\n', ''))
        logs.other()
    finally:
        chrome.drop_screen(file_name)
        send.send(file_name)
        times.sleep()


if __name__ == '__main__':
    while True:
        main()
