import os, sys, glob

path = os.path.dirname(os.path.realpath(__file__))
parentpath = os.path.abspath(os.path.join(path, os.pardir))
modulespath = parentpath + '\Modules'
os.chdir(modulespath)
sys.path.append(modulespath)
name_test = r'КПЭ ВС (ФКУ): ЛК ИП'

from GetLog import Logs
from Chrome import Browser, downloadDir
from ZabbixSend import ZabbixSend
from Times import Times
from Settings import LK_IP_User, LK_IP_Pass
from Files import open_and_read_PDF, delete_file
from selenium.common.exceptions import NoSuchElementException


# Переменная окружения - от значения зависит место хранения логов, варианты 'dev' и 'prom'
env = 'prom'

#Web
url = r'https://lkip2.nalog.ru/lk#!/login'
chrome = Browser(url, env)
file_name = os.path.splitext(os.path.basename(__file__))[0]

# Создаем экземпляры классов Zabbix, Logs и Times
send = ZabbixSend(env)
logs = Logs(file_name, env)
times = Times()

def back_on_mainpage():
    """ Возврат на главную страницу
        Проверка отображения блоков """
    chrome.click_link_text('Главная')
    chrome.browser.find_element_by_id('index-react')

def startAndAutorization():
    """ Чистка лога в CMD
        Запуск браузера в полноэкранном режиме
        Неявное ожидание элементов до 60 секунд
        Переход на страницу сервиса
        Проверка отображения страницы авторизации, строки Логин и пароль
        Ввод Логина
        Ввод Пароля
        Нажатие кнопки 'Вход' '"""
    logs.clear()
    tmp_msg = f'### Вход на сервис {url} ###, Страница авторизации, Авторизация под "{LK_IP_User}"'
    chrome.start()
    chrome.browser.implicitly_wait(60)
    try:
        chrome.get_url()
    except:
        raise Exception(f'### Cервис {url} недоступен ###')

    try:
        chrome.browser.find_element_by_link_text("Логин и пароль")
    except:
        raise Exception('Не отображается Страница авторизации')

    try:
        chrome.browser.find_element_by_name('username').send_keys(LK_IP_User)
        chrome.browser.find_element_by_name('password').send_keys(LK_IP_Pass)
        times.sleep(1)
        chrome.click_link_text("Войти")
    except:
        raise Exception(f'Не выполнена Авторизация под "{LK_IP_User}"')
    logs.info(tmp_msg)

def main_page():
    """ Закрытие всплывающего окна
        Проверка отображения главной страницы авторизированного пользователя"""
    tmp_msg = 'Главная страница авторизированного пользователя'
    try:
        try:
            chrome.click_class('x-btn-icon-el.icon.icon__small-blue-arrow-up')
        except:
            pass
        chrome.text_xpath("//*[contains(text(), '100000000002')]") #Проверка ИНН
        chrome.text_xpath("//*[contains(text(), '100000000000002')]") #Проверка ОГРНИП
        chrome.text_xpath("//*[contains(text(), 'Жизненные ситуации')]") #Проверка блока "Услуги. Сервисы - Жизненные ситуцаии"
    except:
        raise Exception(tmp_msg)
    logs.info(tmp_msg)

def ens():
    """ Пауза 1 секунду
        Нажатие на кнопку `Пополнить`
        Клик по элементу 'Сформировать квитанцию на оплату'
        Пауза 0,5 секунд
        Ввод суммы пополнения `500`
        Нажатие на кнопку `Сформировать квитанцию на оплату`
        Возврат на главную страницу """
    tmp_msg = 'Раздел ЕНС'
    try:
        times.sleep(1)
        chrome.click_xpath('//div[@data-testid="paymentSumContainer"]/div[2]/button')
        chrome.browser.find_element_by_class_name('stg-segmented-control__item')
        chrome.elements_class_click('stg-segmented-control__item', 1)
        times.sleep(0.5)
        chrome.browser.find_element_by_id('sum').send_keys('500')
        chrome.click_name('receiptPayment')
        chrome.click_class('stg-icon-lkul-arrow-left')
        chrome.browser.find_element_by_id('index-react')
    except:
        raise Exception(tmp_msg)
    logs.info(tmp_msg)

def open_pdf():
    """ Пауза 7 секунд
        Проверка скачивания документа
        Открытие pdf
        Проверка наличия текста '500'
        Удаление всех скаченных документов, содержащих в имени ''Платежный_документ' """
    tmp_msg = 'Проверка скачивания документа, Отображение печатной формы платежного документа'
    try:
        times.sleep(7)
        file = f'Платежный_документ*'
        fileList = glob.glob(downloadDir + r'\\' + file)
        if len(fileList) == 0:
            raise Exception('Документ не скачан')
    except:
        raise Exception(tmp_msg)

    try:
        open_and_read_PDF('Платежный_документ*', 0, "500") # Имя, страница, текст
        delete_file('Платежный_документ*', many=True) # Удаление
    except:
        raise Exception('Не отображается печатная форма платежного документа')
    logs.info(tmp_msg)

def nalog_calculator():
    """ Нажатие на 'Налоговый калькулятор'
        Проверка отображения формы ввода реквизитов """
    tmp_msg = 'Раздел "Налоговый калькулятор"'
    try:
        chrome.click_xpath("//span[text()='Налоговый калькулятор']")
        chrome.browser.find_element_by_class_name('x-form-cb-label.x-form-cb-label-after')
        checkbox = chrome.browser.find_elements_by_class_name('x-form-cb-label.x-form-cb-label-after')
        assert len(checkbox) == 3
    except:
        raise Exception(tmp_msg)
    logs.info(tmp_msg)

def all_life_situations():
    """ Возврат на главную страницу
        Пауза 2 секунды
        Нажатие на 'Показать все'
        Проверка отображения страницы с перечнем сервисов
        Проверка отображения блоков
        'Запросить справки и другие документы' и 'Прочие жизненные ситуации' """
    tmp_msg = 'Раздел "Жизненные ситуации"'
    try:
        back_on_mainpage()
        times.sleep(2)
        chrome.click_xpath("//*[contains(text(), 'Показать все')]")
        chrome.browser.find_element_by_class_name('x-box-inner ')
        chrome.browser.find_element_by_xpath("//a[@href='#/situations/ens/information/service']") #Запросить справки и другие документы
        chrome.browser.find_element_by_xpath("//a[@href='#/situations/ens/other']") #Прочие жизненные ситуации
    except:
        raise Exception(tmp_msg)
    logs.info(tmp_msg)

def events():
    """ Возврат на главную страницу
        Нажатие на 'События'
        Проверка отображения страницы 'Предстоящие события' """
    tmp_msg = 'Раздел "События"'
    try:
        back_on_mainpage()
        element = chrome.browser.find_element_by_class_name('white-circle')
        chrome.browser.execute_script("arguments[0].click();", element)
        chrome.browser.find_element_by_id('content-container')
        chrome.text_xpath("//*[contains(text(), 'Предстоящие события')]")
    except:
        raise Exception(tmp_msg)
    logs.info(tmp_msg)

def help():
    """ Нажатие на кнопку 'Меню'
        Нажатие кнопки 'Помощь'
        Проверка отображения страницы 'Помощь' """
    tmp_msg = 'Раздел "Помощь"'
    try:
        chrome.click_xpath("//span[@data-tip-over='Меню']")
        chrome.click_link_text('Помощь')
        ##Проверка отображения частых вопросов
        chrome.text_xpath(
            "//*[contains(text(), 'Как отменить некорректно заполненное уведомление об исчисленных суммах?')]")
        chrome.text_xpath(
            "//*[contains(text(), 'Если допущена ошибка в реквизитах при предоставлении уведомления об исчисленных суммах налогов, то')]")
    except:
        raise Exception(tmp_msg)
    logs.info(tmp_msg)

def reest_subjects_MSP():
    """ Возврат на главную страницу
        Пауза 1 секунда
        Нажатие на 'Реестр субъектов МСП'
        Пауза 4 секунды
        Переход на открывшуюся вкладку
        Проверка перехода на сервис 'https://rmsp.nalog.ru/'
        Закрытие вкладки """
    tmp_msg = 'Раздел "Реестр субъектов МСП"'
    try:
        back_on_mainpage()
        times.sleep(1)
        chrome.click_class('stg-icon-lkip-msp-subjects')
        times.sleep(4)
        window_name_closed = chrome.browser.window_handles[-1]
        chrome.browser.switch_to.window(window_name=window_name_closed)

        chrome.serv_url('https://rmsp.nalog.ru/')
        chrome.browser.close()
        chrome.browser.switch_to.window(
        window_name=chrome.browser.window_handles[-1])
    except:
        raise Exception(tmp_msg)
    logs.info(tmp_msg)

def exit_lkip():
    """ Нажатие на кнопку 'Меню'
        Неявное ожидание элементов до 15 секунд
        Нажатие кнопки 'Выход'
        Нажатие кнопки "Да"
        Проверка успешного выхода из личного кабинета"""
    tmp_msg = 'Выход из личного кабинета'
    try:
        chrome.click_xpath("//span[@data-tip-over='Меню']")
        chrome.browser.implicitly_wait(15) #Неявное ожидание
        chrome.click_link_text('Выход')
        chrome.click_link_text('Да')
        chrome.waitfor_tag_name_el('h2')
    except:
       raise Exception(tmp_msg)
    chrome.quit()
    logs.info(tmp_msg)
    logs.other()

def main():
    """Последовательно запускаем все вышеперечисленные функции
    Если в какой-то из них происходит ошибка, то делаем скрин, пишем лог с ошибкой
    В любом случае чистим старые скриншоты, отправляем файл в заббикс и ставим функцию на паузу"""
    try:
        startAndAutorization()
        main_page()
        ens()
        open_pdf()
        nalog_calculator()
        all_life_situations()
        events()
        help()
        reest_subjects_MSP()
        exit_lkip()
    except Exception as err:
        chrome.screen(file_name)
        print('ОШИБКА - ' + str(err).replace('\n', ''))
        chrome.quit()
        logs.error(str(err).replace('\n', ''))
        logs.other()
    finally:
        chrome.drop_screen(file_name)
        send.send(file_name)
        times.sleep()

if __name__ == '__main__':
    while True:
        main()
