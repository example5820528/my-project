import os, sys

path = os.path.dirname(os.path.realpath(__file__))
parentpath = os.path.abspath(os.path.join(path, os.pardir))
modulespath = parentpath + '\Modules'
os.chdir(modulespath)
sys.path.append(modulespath)
name_test = r'КПЭ ВнС (ФКУ): Реестр систем'

from GetLog import Logs
from Chrome import Browser
from ZabbixSend import ZabbixSend
from Times import Times
from Settings import Sr_User, Sr_Pass

# Переменная окружения - от значения зависит место хранения логов, варианты 'dev' и 'prom'
env = 'prom'

# Web
url = r'https://sr.dpc.tax.nalog.ru/'
chrome = Browser(url, env)
file_name = os.path.splitext(os.path.basename(__file__))[0]

# Создаем экземпляры классов Zabbix, Logs и Times
send = ZabbixSend(env)
logs = Logs(file_name, env)
times = Times()

name_object = '' #Переменная для хранения наименования объекта

def if_else(expectation, fact, text_msg, locator):
    """
        expectation - Ожидаемый результат
        fact - Фактический результат
        text_msg - текст переменной 'tmp_msg'
        locator - локатор, по которому проверка после 'else'
    """
    if expectation != fact:
        tmp_msg = text_msg
        raise Exception(tmp_msg)
    else:
        locator

def start():
    """ Чистка логов в cmd
        Запуск браузера в полноэкранном режиме
        Переход на страницу сервиса
        Проверка отображения поля для ввода Логина """
    logs.clear()
    tmp_msg = f'###### Вход на сервис {url} ######'
    chrome.start()
    try:
        chrome.get_url()
        times.sleep(2)
        chrome.get_url()
        chrome.waitfor_id_el('username', 30)
    except:
        raise Exception(f'### Cервис {url} недоступен ###')
    logs.info(tmp_msg)

def autorization():
    """ Ввод логина
        Ввод пароля
        Нажатие 'Войти'
        Проверка отображения раздела 'Объекты` с таблицей объектов """
    tmp_msg = f'Авторизация под УЗ {Sr_User}'
    global name_object  #Глобальная для того, чтобы изменить значение переменной в след функции
    try:
        chrome.browser.implicitly_wait(15) #Неявное ожидание элементов

        chrome.browser.find_element_by_id('username').send_keys(Sr_User)
        chrome.browser.find_element_by_id('password').send_keys(Sr_Pass)
        chrome.click_id('kc-login')
        chrome.text_xpath('//*[contains(text(), "Объект")]')

        #Меняем на текст объекта из таблицы (без конкретной привязки, т.к. элементы могут меняться в реестре)
        name_object = chrome.browser.find_element_by_xpath(
            '//td[@class="fns-table__tableCell"]/a/div/div/p').text
    except:
        raise Exception(tmp_msg)
    logs.info(tmp_msg)

def object():
    """ Выбор первого объекта в таблице
        Проверка отображения имени объекта и содержимого """
    tmp_msg = 'Выбор объекта'
    try:
        chrome.click_xpath('//td[@class="fns-table__tableCell"]/a') #Клик на первый объект в таблице
        titile_name_object = chrome.browser.find_element_by_tag_name('h3').text #Запись имени страницы объекта
        locator = chrome.waitfor_class_name_el('fns-tabs__tabPanel', 15) #Запись локатора проверки содержимого страницы объекта и перехват неявного ожидания

        if_else(name_object, titile_name_object, 'Страница объекта не совпадает с выбранным объектом', locator)
    except:
        raise Exception(tmp_msg)
    logs.info(tmp_msg)

def structura():
    """ Нажатие на 'Структура' в верхнем меню
        Проверка раздела "Классы" со списком классов"""
    tmp_msg = 'Гиперссылка "Структура"'
    try:
        chrome.browser.implicitly_wait(15)  #Неявное ожидание элементов
        chrome.click_xpath('//a[@href="/structure"]') #Клик 'Структура'
        page_class = 'Классы' #Запись в переменную ожидаемого результата
        title_page = chrome.browser.find_element_by_tag_name('h3').text #Запись имени страницы класса
        locator = chrome.browser.find_element_by_xpath(
            '//div[@data-testid="ClassTable_table"]') #Ждем список классов, которые хранятся в данном локаторе (Классы-Class))

        if_else(page_class, title_page, 'Гиперссылка "Структура" не ведет на "Классы"', locator)
    except:
        raise Exception(tmp_msg)
    logs.info(tmp_msg)

def random_class():
    """ Выбор первого класса в таблице
        Проверка отображения имени класса и содержимого """
    tmp_msg = 'Проверка класса'
    try:
        class_name = chrome.browser.find_element_by_xpath(
            '//td[@class="fns-table__tableCell"]/a/div/div/p').text #Текст класса из таблицы
        chrome.click_xpath('//td[@class="fns-table__tableCell"]/a') #Клик на первый класс в таблице
        titile_name_class = chrome.browser.find_element_by_tag_name('h3').text #Запись имени страницы класса
        locator = chrome.browser.find_element_by_class_name('fns-table__bodyContainer') #Ждем список атрибутов класса

        if_else(class_name, titile_name_class, 'Страница класса не совпадает с выбранным классом', locator)
    except:
        raise Exception(tmp_msg)
    logs.info(tmp_msg)

def atribut():
    """ Выбор первого атрибута в таблице
        Проверка отображения имени атрибута и содержимого """
    tmp_msg = 'Проверка атрибута'
    try:
        atribut_name = chrome.browser.find_element_by_xpath("//td[@class='fns-table__tableCell']/a").text #Текст атрибута из таблицы
        chrome.click_xpath('//td[@class="fns-table__tableCell"]/a') #Клик на первый атрибут в таблице
        titile_name_atribut = chrome.browser.find_element_by_tag_name('h3').text #Запись имени страницы атрибута
        locator = chrome.text_xpath('//*[contains(text(), "Связь")]') #Ждем страницу выбранного атрибута

        if_else(atribut_name, titile_name_atribut, 'Страница атрибута не совпадает с выбранным атрибутом', locator)
    except:
        raise Exception(tmp_msg)
    logs.info(tmp_msg)

def search():
    """ Ввод в поисковой строке 'сервис'
        Нажатие на 'Показать все результаты'
        Проверка отображения результата и вкладок """
    tmp_msg = 'Поиск сервисов'
    try:
        seach_input = '«сервис»' #Ожидаем, что результат будет соответсвовать значению переменной

        chrome.browser.find_element_by_class_name(
            'fns-textField__input').send_keys('сервис') #В поле поиска ввод 'сервис'
        chrome.click_xpath('//button[@data-testid="Search_showResults-button"]') #'Показать все рез-ты'
        res_search = chrome.browser.find_element_by_xpath(
            '//h4[@data-testid="AllResults_searchValue"]').text #Результат поиска
        locator = chrome.text_xpath('//*[contains(text(), "Все")]')

        if_else(seach_input, res_search, 'Результат поиска не совпадает с введенным значением', locator)
        #Проверка наличия вкладок группировки
        chrome.text_xpath('//*[contains(text(), "Классы")]')
        chrome.text_xpath('//*[contains(text(), "Объекты")]')
        chrome.text_xpath('//*[contains(text(), "Пакеты изменений")]')
    except:
        raise Exception(tmp_msg)
    logs.info(tmp_msg)

def pack():
    """ Нажатие на 'Пакеты изменений'
        Выбор пакета
        Првоерка отображения карточки пакета и внесенных изменений """
    tmp_msg = 'Карточка пакета'
    try:
        chrome.click_xpath('//div[@data-testid="AllResults"]/div[2]/button[4]') #Клик 'Пакеты изменений'
        pack_name = chrome.browser.find_element_by_xpath('//a[@data-testid="AllResults_value-link"]').text #Ожидаем, что результат будет соответсвовать значению переменной
        chrome.click_xpath('//a[@data-testid="AllResults_value-link"]') #Клик на пакет
        title_pack_name = chrome.browser.find_element_by_tag_name('h3').text #Запись имени страницы пакета
        locator = chrome.browser.find_element_by_xpath('//div[@data-testid="ChangesTable"]') #Таблица

        if_else(pack_name, title_pack_name, 'Открытая карточка не совпадает с выбранным пакетом', locator)
    except:
        raise Exception(tmp_msg)
    logs.info(tmp_msg)

def agreement():
    """ Нажатие в правом верхнем углу меню 'Пакеты изменений'
        Переход в раздел 'Согласовано'
        Проверка отображения согласованных пакетов всех пользователей"""
    tmp_msg = 'Вкладка "Согласовано"'
    try:
        chrome.click_xpath('//a[@href="/packages"]')  #Клик 'Структура'
        chrome.browser.find_element_by_xpath('//div[@class="fns-tabs__tabList"]/button[2]').click() #Клик 'Согласовано'
        chrome.text_xpath('//*[contains(text(), "Согласовано")]') #Ищет текст (не важно имя таблицы либо колонка таблицы)
        chrome.text_xpath('//*[contains(text(), "Наименование пакета")]') #Ищет текст колонки таблицы
        chrome.browser.find_elements_by_class_name('fns-table__tableRow') #Проверяет все доступные строки таблицы
    except:
        raise Exception(tmp_msg)
    logs.info(tmp_msg)

def exit():
    tmp_msg = 'Выход'
    chrome.quit()
    logs.info(tmp_msg)
    logs.other()

def main():
    """Последовательно запускаем все вышеперечисленные функции
    Если в какой-то из них происходит ошибка, то делаем скрин, пишем лог с ошибкой
    В любом случае чистим старые скриншоты, отправляем файл в заббикс и ставим функцию на паузу"""
    try:
        start()
        autorization()
        object()
        structura()
        random_class()
        atribut()
        search()
        pack()
        agreement()
        exit()
    except Exception as err:
        chrome.screen(file_name)
        print('ОШИБКА - ' + str(err).replace('\n', ''))
        chrome.quit()
        logs.error(str(err).replace('\n', ''))
        logs.other()
    finally:
        chrome.drop_screen(file_name)
        send.send(file_name)
        times.sleep()

if __name__ == '__main__':
    while True:
        main()
