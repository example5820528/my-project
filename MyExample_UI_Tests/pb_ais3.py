import os, sys

path = os.path.dirname(os.path.realpath(__file__))
parentpath = os.path.abspath(os.path.join(path, os.pardir))
modulespath = parentpath + '\Modules'
os.chdir(modulespath)
sys.path.append(modulespath)
name_test = r'КПЭ ВнС (ФКУ): Прозрачный бизнес'

from GetLog import Logs
from Chrome import Browser
from ZabbixSend import ZabbixSend
from Times import Times
from Files import open_and_read_PDF, delete_file
from selenium.common.exceptions import NoSuchElementException

env = 'prom'

#Web
url = r'https://pb.ais3.tax.nalog.ru/'
chrome = Browser(url, env)
file_name = os.path.splitext(os.path.basename(__file__))[0]

#Zabbix, logs, times
send = ZabbixSend(env)
logs = Logs(file_name, env)
times = Times()

def title_mainpage():
    """
        Проверка страницы по заголовку
        Возрват на главную страницу
    """
    chrome.waitfor_tag_name_el('h1')
    chrome.click_id('mnuIndex')

def frame_link(nameframe, text_link):
    """
        Активация frame
        Клик link_text
        Деактивация frame
    """
    chrome.browser.switch_to.frame(nameframe)
    chrome.click_link_text(text_link)
    chrome.browser.switch_to.default_content()

def start():
    """
        Чистка лога в CMD
        Запуск браузера в полноэкранном режиме
        Переход на страницу сервиса
        В окне 'Этот веб-сайт не защищен' нажатие на 'Подробнее - Перейти на веб-страницу'
    """
    logs.clear()
    tmp_msg = f'###### Вход на сервис {url} ######'
    chrome.start('chrome_option', False)
    try:
        chrome.get_url()
        times.sleep(2)
        chrome.get_url()
    except:
        raise Exception(f'### Cервис {url} недоступен ###')
    logs.info(tmp_msg)

def firstPage():
    """
        Проверка прогрузки главной страницы
    """
    tmp_msg = 'Главная страница'
    try:
        chrome.waitfor_class_name_el('pnl-search', 20)
    except:
        raise Exception(tmp_msg)
    logs.info(tmp_msg)

def transitionTabs():
    """
        Последовательный переход по вкладкам
        'О Сервисе'
        Отображение страницы 'О Сервисе'
        Возврат на главную страницу
        'Коррекция информации о моей компании'
        Отображение страницы 'Коррекция информации о моей компании'
        Возврат на главную страницу
        'Вопросы и ответы'
        Отображение страницы 'Вопросы и ответы'
        Возврат на главную страницу
        Видеоматериалы'
        Отображение страницы 'Видеоматериалы'
    """
    tmp_msg = 'Переход по вкладкам'
    try:
        chrome.browser.implicitly_wait(15)
        # О сервисе - проверка - на главную страницу
        chrome.click_xpath("//a[@href='about.html']")
        title_mainpage()

        # Коррекция инфо. моей компании - проверка - на главную страницу
        chrome.click_xpath("//a[@href='feedback.html']")
        title_mainpage()

        # Вопросы и ответы - проверка - на главную страницу
        chrome.click_xpath("//a[@href='qa.html']")
        title_mainpage()

        # Видеоматериалы - проверка - на главную страницу
        chrome.click_xpath("//a[@href='video.html']")
        chrome.waitfor_tag_name_el('h1')
    except:
        raise Exception(tmp_msg)
    logs.info(tmp_msg)

def calculator():
    """
        Нажатие 'Калькулятор налоговой нагрузки'
        Отображение страницы 'Налоговый калькулятор'
        Ввод параметров расчета:
        Налоговый период - 2021
        Вид экономической деятельности - 11-Производство напитков
        Субъект РФ - г.Москва
        Масштаб деятельности - Малое предприятие, от 120 д0 500 млн.руб
        Нажатие "Показать среднеотраслевые индикаторы"
        Отображение окна "Налоговая нагрузка"
        Нажатие "Закрыть"
        Переход на главную страницу
    """
    tmp_msg = 'Калькулятор налоговой нагрузки'
    try:
        chrome.waitfor_click_xpath_and_click("//a[@href='calculator.html']", 15)
        chrome.waitfor_tag_name_el('h1', 15)
        # Налог период - 2021
        chrome.browser.find_element_by_id('uni_select_3').send_keys('2021')
        chrome.down()
        chrome.enter()
        # Вид эко деятельности - 11
        chrome.click_id('uni_set_0')
        frame_link('uniDialogFrame', '11 - Производство напитков')
        # Субъект РФ - г.Москва
        times.sleep(16)
        chrome.click_id('uni_set_2')
        frame_link('uniDialogFrame', 'Г.Москва')
        # Масштаб деятельности - Малое
        chrome.click_id('uni_set_4')
        chrome.browser.switch_to.frame('uniDialogFrame')
        chrome.elements_class_click('btn-node', 1)
        chrome.click_link_text('от 120 до 500 млн.руб')
        chrome.browser.switch_to.default_content()
        times.sleep(15)
        #Клик - Показать среднеотр. индикаторы
        chrome.click_id('btnCalcShow')
        times.pause(5)
        chrome.browser.switch_to.frame('uniDialogFrame')
        times.pause(5)
        chrome.waitfor_id_el('dialogTitle')
        chrome.click_xpath('//*[@id="btnCancel"]')
        chrome.browser.switch_to.default_content()
        chrome.click_id('mnuIndex')
    except:
        raise Exception(tmp_msg)
    logs.info(tmp_msg)

def checkContractor():
    """
        Отображение главной страницы "Проверь себя и контрагента"
        Ввод ИНН 7733840910 в поле 'Введите, например, ИНН' и нажатие кнопки 'Поиск'
        Отображение результатов поиска 'ООО МСП-КОНСАЛТИНГ'
        Нажатие 'ООО МСП-КОНСАЛТИНГ'
        Отображение окна с подробной информацией 'ОБЩЕСТВО С ОГРАНИЧЕННОЙ ОТВЕТСТВЕННОСТЬЮ 'МСП-КОНСАЛТИНГ'
    """
    tmp_msg = 'Проверка контрагента по ИНН'
    try:
        chrome.waitfor_click_class_name_and_click('pnl-search', 25)
        chrome.browser.find_element_by_name('queryAll').send_keys('7733840910')
        times.sleep(15)
        chrome.click_id('quickSubmit')
        chrome.waitfor_click_class_name_and_click('lnk.company-info', 80)
        chrome.waitfor_xpath('//*[@id="pnlCompany"]/div[1]/div[1]/div/div[2]/div[2]/a', 'ОБЩЕСТВО С ОГРАНИЧЕННОЙ ОТВЕТСТВЕННОСТЬЮ "МСП-КОНСАЛТИНГ"', 60)
    except:
        raise Exception(tmp_msg)
    logs.info(tmp_msg)

def downloadEGTUL():
    """ Проверка активности ссылки для скачивания 'Получить выписку из ЕГРЮЛ' """
    tmp_msg = 'Проверка возможности скачивания "Выписки из ЕГРЮЛ"'
    try:
        times.sleep(15)
        chrome.click_class('lnk-egrl.lnk-service')
        times.sleep(18)
    except:
        raise Exception(tmp_msg)
    logs.info(tmp_msg)

def open_pdf():
    """
        Проверка скачивания документа
        Открытие pdf
        Проверка наличия текста
        Удаление всех скаченных документов, содержащих в имени '7733840910'
    """
    tmp_msg = 'Отображение печатной формы платежного документа'
    try:
        open_and_read_PDF('7733840910*.pdf', 0, 'ОБЩЕСТВО С ОГРАНИЧЕННОЙ ОТВЕТСТВЕННОСТЬЮ "МСП-КОНСАЛТИНГ"')
        delete_file('7733840910*.pdf', True)
    except:
        raise Exception(tmp_msg)
    logs.info(tmp_msg)

def info_sum():
    """
        Нажатие на блок 'Сведения о суммах недоимки и задолженности по пеням и штрафам
        Проверка отображения блока'
    """
    tmp_msg = 'Блок "Сведения о суммах недоимки и задолженности по пеням и штрафам"'
    try:
        chrome.elements_class_click('lnk-container', 6)
        #Чтобы проверить, что блок раскрылся - нажатие после раскрытия на год '2019', так как по html таблица доступна всегда (не важно открыли блок или нет)
        chrome.click_link_text('2019')
    except:
        raise Exception(tmp_msg)
    logs.info(tmp_msg)

def exit():
    tmp_msg = 'Выход'
    chrome.quit()
    logs.info(tmp_msg)
    logs.other()

def main():
    """
        Последовательно запускаем все вышеперечисленные функции
        Если в какой-то из них происходит ошибка, то делаем скрин, пишем лог с ошибкой
        В любом случае чистим старые скриншоты, отправляем файл в заббикс и ставим функцию на паузу
    """
    try:
        start()
        firstPage()
        transitionTabs()
        calculator()
        checkContractor()
        downloadEGTUL()
        open_pdf()
        info_sum()
        exit()
    except Exception as err:
        chrome.screen(file_name)
        print('ОШИБКА - ' + str(err).replace('\n', ''))
        chrome.quit()
        logs.error(str(err).replace('\n', ''))
        logs.other()
    finally:
        chrome.drop_screen(file_name)
        send.send(file_name)
        times.sleep(250)

if __name__ == '__main__':
    while True:
        main()
