import os, sys, glob

path = os.path.dirname(os.path.realpath(__file__))
parentpath = os.path.abspath(os.path.join(path, os.pardir))
modulespath = parentpath + '\Modules'
os.chdir(modulespath)
sys.path.append(modulespath)
name_test = r'КПЭ ВС (ФКУ): Информационные стенды'

from GetLog import Logs
from Chrome import Browser, downloadDir
from ZabbixSend import ZabbixSend
from Times import Times
from Files import delete_file

# Переменная окружения - от значения зависит место хранения логов, варианты 'dev' и 'prom'
env = 'prom'

# Web
url = r'https://stand.nalog.ru'
chrome = Browser(url, env)
file_name = os.path.splitext(os.path.basename(__file__))[0]

# Создаем экземпляры классов Zabbix, Logs и Times
send = ZabbixSend(env)
logs = Logs(file_name, env)
times = Times()

def input():
    """ Функция ввода данных и нажатия `Enter` """
    chrome.waitfor_click_class_name_and_click("select2-selection__placeholder", 10)
    chrome.enter()

def start():
    """
        Чистка логов в cmd
        Запуск браузера в полноэкранном режиме
        Переход на страницу сервиса
    """
    logs.clear()
    tmp_msg = f'###### Вход на сервис {url} ######'
    chrome.start(browser='chrome_option')
    try:
        chrome.get_url()
        times.sleep(2)
        chrome.get_url()
        chrome.waitfor_tag_name_el('h1', 10)
    except:
        raise Exception(f'### Cервис {url} недоступен ###')
    logs.info(tmp_msg)

def input_region():
    """
    Выбор региона - Первый в выпадающем списке
    Выбор ИФНС - Первый в выпадающем списке
    Выбор Категории стендов - Первый в выпадающем списке
    """

    tmp_msg = 'Ввод данных'
    try:
        input()
        input()
        input()
    except:
        raise Exception(tmp_msg)
    logs.info(tmp_msg)

def result():
    """"""
    tmp_msg = 'Проверка отображения стендов'
    try:
        chrome.waitfor_xpath_el('//*[contains(text(),"Выбор подходящего режима налогообложения")]', 15)
    except:
        raise Exception(tmp_msg)
    logs.info(tmp_msg)

def download_delete_pdf():
    """
    Нажатие на 'PDF' 'Выбор подходящего режима налогообложения'
    Ожидание 3 секундЫ
    Проверка наличия файла в папке загрузок
    Удаление всех файлов содержащих 'VRN_02'
    """
    tmp_msg = 'Проверка возможности скачивания документа'
    try:
        chrome.click_xpath("//table[@class='table']/tbody/tr[1]/td[2]/a")
        times.sleep(3)
        file = f'*VRN_02.pdf'
        fileList = glob.glob(downloadDir + r'\\' + file)
        if len(fileList) == 0:
            raise Exception()
        delete_file('*VRN_02.pdf', many=True) # Удаление
    except:
        raise Exception(tmp_msg)
    logs.info(tmp_msg)

def exit():
    tmp_msg = 'Выход'
    chrome.quit()
    logs.info(tmp_msg)
    logs.other()

def main():
    """Последовательно запускаем все вышеперечисленные функции
    Если в какой-то из них происходит ошибка, то делаем скрин, пишем лог с ошибкой
    В любом случае чистим старые скриншоты, отправляем файл в заббикс и ставим функцию на паузу"""
    try:
        start()
        input_region()
        result()
        download_delete_pdf()
        exit()
    except Exception as err:
        chrome.screen(file_name)
        print('ОШИБКА - ' + str(err).replace('\n', ''))
        chrome.quit()
        logs.error(str(err).replace('\n', ''))
        logs.other()
    finally:
        chrome.drop_screen(file_name)
        send.send(file_name)
        times.sleep()

if __name__ == '__main__':
    while True:
        main()
