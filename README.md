<h1 align="center">Привет, меня зовут <a href="https://t.me/Vadim_nbs" target="_blank">Вадим</a> 
<img src="https://github.com/blackcater/blackcater/raw/main/images/Hi.gif" height="32"/></h1>
<h3 align="center">Тестировщик-автоматизатор на Python</h3>  

Мой стек:  

![Python](https://img.shields.io/badge/python-3670A0?style=for-the-badge&logo=python&logoColor=ffdd54) 
![Selenium](https://img.shields.io/badge/-selenium-%43B02A?style=for-the-badge&logo=selenium&logoColor=white)
![GitLab](https://img.shields.io/badge/gitlab-%23181717.svg?style=for-the-badge&logo=gitlab&logoColor=white)
![Grafana](https://img.shields.io/badge/grafana-%23F46800.svg?style=for-the-badge&logo=grafana&logoColor=white)
![Docker](https://img.shields.io/badge/docker-%230db7ed.svg?style=for-the-badge&logo=docker&logoColor=white)
![Jira](https://img.shields.io/badge/jira-%230A0FFF.svg?style=for-the-badge&logo=jira&logoColor=white)
![Confluence](https://img.shields.io/badge/confluence-%23172BF4.svg?style=for-the-badge&logo=confluence&logoColor=white)
![Windows](https://img.shields.io/badge/Windows-0078D6?style=for-the-badge&logo=windows&logoColor=white)
![Linux](https://img.shields.io/badge/Linux-FCC624?style=for-the-badge&logo=linux&logoColor=black)  

![line](https://capsule-render.vercel.app/api?type=rect&color=gradient&height=1)  

Проект создан для демонстрации моих [UI](https://gitlab.com/example5820528/my-project/-/tree/main/MyExample_UI_Tests) скриптов тестирования и примера [рабочих задач вместе с активностью](https://gitlab.com/example5820528/my-project/-/tree/main/MyExample_Issue_job)

![line](https://capsule-render.vercel.app/api?type=rect&color=gradient&height=1) 

[![Yandex](https://img.shields.io/badge/-VadimNebel@yandex.ru-F9DB60?style=flat-square&logo=Yandex&logoColor=FF3333)](mailto:VadimNebel@yandex.ru)
[![Telegram](https://img.shields.io/badge/Telegram-blue?style=flat-square&logo=Telegram)](https://t.me/Vadim_nbs) 
[![hh](https://img.shields.io/badge/-hh.ru-red?style=flat-square&logo=hh&logoColor=red)](https://krasnoyarsk.hh.ru/resume/36cfae30ff08582d2d0039ed1f7a5942577553)
 


